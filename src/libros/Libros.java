package libros;
import java.util.Scanner;
/**
 *
 * @author Ase Ü 
 */
public class Libros {
    String titulo;
    String autor;
    int paginas;
    
    //Constructor por default
    Libros(){
        this.titulo = "Mi libro favorito";
        this.autor = "Anónimo";
        this.paginas = 1000;
    }
    
    //Constructor por parámetros
    Libros(String t,String a,int p){
        this.titulo = t;
        this.autor = a;
        this.paginas = p;
    }
   
    private String getTitulo(){
        return this.titulo;
    }
    
    private String getAutor(){
        return this.autor;
    }
    
    private int getPaginas(){
        return this.paginas;
    }
  
    private void setTitulo(String t){
        this.titulo=t;
    }
    
    private void setAutor(String a){
        this.autor=a;
    }
    
    private void setPaginas(int p){
        this.paginas=p;
    }
    
    private void show(){
        System.out.println("Libro: >"+getTitulo()+"<\nAutor: "+getAutor()+"\nPáginas: "+getPaginas());
    }

    //Método que te dice cuantas paginas leer para terminar el libro en una semana
    public void leerPaginas(){
        int p;
        p=getPaginas()/7;
        System.out.printf("Para terminar de leer el libro en una semnana necesitas leer %d páginas diarias.\n", p);
    }
    
    //Método que te dice cuantos días te faltan para terminar el libro
    public void faltanDias(){
        int p,r;
        Scanner teclado=new Scanner(System.in);
        System.out.println("¿Cuántos días llevas leyendo?");
        p=teclado.nextInt();
        if(p>0&&p<=7){
            System.out.printf("Te faltan %d día(s) para terminar el libro\n", r=7-p);
        }
        else{
            if(p>7){
                System.out.printf("Tendrías que haber acabado el libro hace %d día(s)\n", r=p-7);
            }
            else{
                System.out.println("¿Cómo le haces para leer en días negativos?");
            }
        }      
    }
    
    //Método que te dice cuantas secuelas del libro hay
    public void existenSecuelas(){
        System.out.println("El libro >"+getTitulo()+"< tiene X secuelas");
    }
    
    //Método que te dice cuantas precuelas del libro hay
    public void existenPrecuelas(){
        System.out.println("El libro >"+getTitulo()+"< tiene Y precuelas");
    }
    
    //Método que dice que otros libros ha escrito el autor
    public void otrosLibros(){
        System.out.println("Otros libros de "+getAutor());
        System.out.println("*Más libros*");
    }
    
    //Método que da autores que escribieron libros parecidos
    public void otrosAutores(){
        System.out.println("Autores que escribieron libros parecidos a >"+getTitulo()+"<");
        System.out.println("*Más autores*");
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner teclado=new Scanner(System.in);
        String tit,aut;
        int pags;
        Libros Q = new Libros();
        Q.show();
        Q.leerPaginas();
        Q.existenPrecuelas();
        Q.existenSecuelas();
        Q.otrosLibros();
        Q.otrosAutores();
        System.out.println("¿Cuál es tu libro favorito?");
        tit=teclado.nextLine();
        System.out.println("¿Quién es su autor?");
        aut=teclado.nextLine();
        System.out.println("¿Cuántas páginas tiene?");
        pags=teclado.nextInt();
        Libros M = new Libros(tit,aut,pags);
        M.leerPaginas();
        M.faltanDias();
        M.existenPrecuelas();
        M.existenSecuelas();
        M.otrosLibros();
        M.otrosAutores();
        M.show();
    }
    
}
